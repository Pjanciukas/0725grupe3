

<!DOCTYPE HTML>
<html>
    <head>
		<link href="https://fonts.googleapis.com/css?family=Lobster+Two" rel="stylesheet">
        <title>Drop</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		
      <script type = "text/javascript">
          function displayNextImage() {
              x = (x === images.length - 1) ? 0 : x + 1;
              document.getElementById("img").src = images[x];
          }

          function displayPreviousImage() {
              x = (x <= 0) ? images.length - 1 : x - 1;
              document.getElementById("img").src = images[x];
          }

          function startTimer() {
              setInterval(displayNextImage, 3000);
          }

          var images = [], x = -1;
          images[0] = "pizza1.jpg";
          images[1] = "pizza2.jpg";
          images[2] = "pizza3.jpg";
      </script>

    </head>
		
    </head>
    <body class="landing">   
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo">
				<?php
session_start();

if($_SESSION["username"] != NULL){
	
	echo "Laba diena ". $_SESSION["username"] . "!" ;
	
}else{
	
	echo "Bad getaway.";
	
}
?>
				</a></h1>
                <nav id="nav">
                    <ul>

                        <li>
                            <a href="#">Recipes</a>
                            <ul>
                                <li><a href="left-sidebar.html">Salads</a></li>
                                <li><a href="right-sidebar.html">Soups</a></li>
                                <li><a href="no-sidebar.html">Pizza</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </header>

            <!-- Banner -->
            <section id="banner">
                <div class="content">
                    <header>
                        <h2>Welcome to recipe Heaven</h2>
                        <p>We hope you have a wonderful day!<br />
                            Eager to try a new recipes?</p>
                    </header>
                    <span class="image"><img id="img" src="welcome" alt="" /></span>
                </div>
            </section>

            <!-- One -->
 

            <!-- Two -->
 

            <!-- Three -->


            <!-- Four -->


            <!-- Five -->
            <section id="five" class="wrapper style2 special">
                <div class="container">
                    <header>
                        <h2> In what kind of recepes are you interested?</h2>
                        <p>At this moment we have: </p>
                    </header>
                    <form class="container 50%">
                        <div class="row uniform 50%">
                            <div class="12u$ 12u$(xsmall)"><a href="left-sidebar.html"><input type="button" value="Salads" class="fit special"/></a></div>
							    <div class="12u$ 12u$(xsmall)"><a  href="right-sidebar.html"><input type="button" value="Soups" class="fit special"/></a></div>
									<div class="12u$ 12u$(xsmall)"><a href="no-sidebar.html"><input type="button" value="Pizzas" class="fit special"/></a></div>
                        </div>
                    </form>
                </div>
            </section>

            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="https://twitter.com" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="https://www.facebook.com" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="https://www.linkedin.com" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="https://www.instagram.com" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="https://github.com" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="https://gmail.com" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>